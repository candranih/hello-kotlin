buildscript {
	repositories {
		gradleScriptKotlin()
	}

	dependencies {
		classpath(kotlinModule("gradle-plugin"))
	}
}

apply {
	plugin("kotlin")
	plugin<ApplicationPlugin>()
}


configure<ApplicationPluginConvention> {
	mainClassName = "com.HelloKt"
}

repositories {
	gradleScriptKotlin()
}

dependencies {
	compile(kotlinModule("stdlib"))
}
